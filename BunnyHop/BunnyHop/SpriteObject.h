#pragma once
#include <SFML/Graphics.hpp>
class SpriteObject
{
public:
	// Constructors / Destructors
	SpriteObject(sf::Texture& newTexture);
	// Functions
	void DrawTo(sf::RenderTarget& target);
	// Getters
	sf::FloatRect GetHitbox();
protected:
	// Data
	sf::Vector2f CalculateCollisionDepth(sf::FloatRect otherHitbox);
	sf::Sprite sprite;
};