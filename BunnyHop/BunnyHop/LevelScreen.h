#pragma once
#include <SFML/Graphics.hpp>
#include "Player.h"
#include "Platform.h"
class Game;

class LevelScreen
{
public:
	LevelScreen(Game* newGamePointer);
	void Input();
	void Update(sf::Time frameTime);
	void DrawTo(sf::RenderTarget& target);
	sf::View camera;
private:
	Player player;
	Platform platform;
	Platform platform2;
	Game* gamePointer;
};