#include "LevelScreen.h"
#include "Game.h"
LevelScreen::LevelScreen(Game* newGamePointer)
	: player(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	, camera(newGamePointer->GetWindow().getDefaultView())
{
}

void LevelScreen::Input()
{
	player.Input();
}

void LevelScreen::Update(sf::Time frameTime)
{
	player.Update(frameTime);
	player.HandleSolidCollision(platform.getHitbox());
}

void LevelScreen::DrawTo(sf::RenderTarget& target)
{
	//Update camera position
	sf::Vector2f currentViewCenter = camera.getCenter();
	float playerCenterY = player.GetHitbox().top + player.GetHitbox().height / 2;
	if (playerCenterY < currentViewCenter.y)
	{
		camera.setCenter(currentViewCenter.x, playerCenterY);
	}

	// Set camera view
	target.setView(camera);

	// Draw content to screen
	player.DrawTo(target);
	platform.DrawTo(target);
	platform2.DrawTo(target);

	//Remove camera view
	target.setView(target.getDefaultView());
}