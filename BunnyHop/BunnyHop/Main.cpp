﻿#include <SFML/Graphics.hpp>
#include "Game.h"
int main()
{
	Game gameInstance;
	// This will not end until the game is over
	gameInstance.RunGameLoop();
	// If we get here, the loop exited, so end the program by returning
	return 0;
}